using Godot;
using System;
using System.Collections.Generic;

/**
 * <summary>
 * 
 * The main glue logic for the game.  In short, there are <see cref="Player"/>s that should be destroying
 * <see cref="Mob"/>s.  To destroy a mob, the player(s) can shoot bullets at them.  Players shoot bullets
 * corresponding to the player color (which can be changed) and some mobs can only be destroyed by the 
 * matching color.  When a mob is destroyed, the score increases.  When the player is destroyed, the number of lives
 * decreases.  When the number of lives reaches zero, the game is over.  There is currently no winning criteria
 * (reach the high score).
 * 
 * This class handles object instantiations, cross-object interactions (e.g. increasing score if <see cref="Player"/>
 * destroys <see cref="Mob"/>, etc.) and the main map.
 * 
 * It also handles bullet creation (since bullets should keep going even if player is defeated) and Mob spawning.
 * 
 * It does a lot and probably shouldn't do as much as it does.
 * </summary>
 */
public class Main : Node
{
	private readonly Random random = new Random();

	/**
	 * Indicates a group can be resized (proof-of-concept implementation, there may be a better way to handle
	 * resizing).
	 */
	public const String RESIZABLE_GROUP_NAME = "resizable";

	/**
	 * <see cref="Node.Name"/> for the manually created label that keeps track of the player score.
	 */
	public const String SCORE_LABEL_NODE_NAME = "ScoreLabel";

	/**
	 * <see cref="Node.Name"/> for the manually created label that keeps track of the player lives.
	 */
	public const String LIVES_LABEL_NODE_NAME = "LivesLabel";

	/**
	 * The maximum number of mobs that are allowed to be alive at any given time.  Limit to not overwhelm player
	 */
	public int maxNumMobs;

	/**
	 * The starting score when the game starts.
	 */
	public const int START_SCORE = 0;

	/**
	 * The number of lives the player has when the game starts.
	 */
	public const int START_LIVES = 3;

	/**
	 * The time in between mob spawns in seconds, if the max number of mobs has not been reached.
	 */
	private int MOB_SPAWN_TIME_S = 2;
	
	/**
	 * Used to construct an instance of <see cref="Player"/>.
	 */
	private static PackedScene playerScene = (PackedScene)GD.Load("res://PlayerScene.tscn");

	/**
	 * Used to construct an instance of <see cref="TriangleMob"/>.
	 */
	private static PackedScene mobScene = (PackedScene)GD.Load("res://mob/triangle/TriangleMobScene.tscn");

	/**
	 * Used to construct an instance of <see cref="Bullet"/>.
	 */
	private static PackedScene bulletScene = (PackedScene)GD.Load("res://BulletScene.tscn");

	/**
	 * Used to construct an instance of <see cref="SquareMob"/>.
	 */
	private static PackedScene squareMobScene = (PackedScene)GD.Load("res://mob/square/SquareMobScene.tscn");

	/**
	 * Used to construct an instance of <see cref="OctagonMob"/>.
	 */
	private static PackedScene octagonMobScene = (PackedScene)GD.Load("res://mob/octagon/OctagonMobScene.tscn");

	private int currentScore = START_SCORE;

	/**
	 * Current number of lives.  When this reaches zero, it's game over.
	 */
	private int currentLives = START_LIVES;

	private int currentNumMobs;

	/**
	 * The <see cref="PathFollow2D"/> that can be used to generate a spawn location on the border of the screen.
	 * NOT THREAD-SAFE.
	 */
	private PathFollow2D mobBorderSpawnFn;

	/**
	 * The timer that controls the time between mob spawns.
	 */
	private Timer mobSpawnTimer;

	/**
	 * The current player using mouse and keyboard.  If no player is using mouse and keyboard, then the value
	 * will be null.  There can be at most one mouse and keyboard player.
	 */
	private Player mousePlayer = null;

	/**
	 * Map of a <see cref="InputEvent.Device"/> to the player that device is mapped to for ONLY the joysticks
	 * (keyboard/mouse is also a device, but is not in this Map).  The reason for the separation is because
	 * handling is very different between the two types of inputs and it is easier to iterate through
	 * only the joysticks in some case.
	 */
	private Dictionary<int, Player> deviceIdToPlayer = new Dictionary<int, Player>();

	/**
	 * A list combining the mouse player with the controller players.  Must be maintained with
	 * <see cref="mousePlayer"/> and <see cref="deviceIdToPlayer"/>.  Mainly as a convenience when iterating
	 * through all players is useful.
	 */
	private List<Player> allPlayers = new List<Player>();

	/**
	 * See <see cref="Globals"/>.
	 */
	private Globals globals;

	private bool isGameOver = false;

	/**
	 * <inheritdoc/>
	 */
	public override void _Ready()
	{
		globals = Globals.LoadGlobals(this);
		// This is mainly for debugging (no player mappings by launching scene directly)
		if (globals.PlayerInputMappings.Count == 0)
		{
			globals.AddPlayerInputMapping(PlayerInputMapping.CreateKeyboardMouseInputMapping(0));
		}
		maxNumMobs = 4 + globals.PlayerInputMappings.Count; // Number of mobs scales with number of players
		GD.Print("Number of players: ", globals.PlayerInputMappings.Count);

		float screenWidth = GetViewport().Size.x;
		float screenHeight = GetViewport().Size.y;

		setupGame(screenWidth, screenHeight);
		resetGame(screenWidth, screenHeight);
	}

	/**
	 * Actions to only perform once when game starts.  Subsequent resets should be performed using
	 * <see cref="resetGame"/>.
	 */
	private void setupGame(float screenWidth, float screenHeight)
	{
		this.GetTree().Root.Connect("size_changed", this, nameof(OnResize));

		mobSpawnTimer = new Timer();
		mobSpawnTimer.Autostart = true;
		mobSpawnTimer.WaitTime = MOB_SPAWN_TIME_S;
		mobSpawnTimer.Connect("timeout", this, nameof(SpawnMob));
		this.AddChild(mobSpawnTimer);

		var mobPathAndSpawnFn = Utils.GenerateBorderSpawnLocations(screenWidth, screenHeight);
		Path2D path2D = mobPathAndSpawnFn.Item1;
		this.AddChild(path2D);
		mobBorderSpawnFn = mobPathAndSpawnFn.Item2;

		Label scoreLabel = new Label();
		scoreLabel.SetPosition(new Vector2(screenWidth / 2, screenHeight * 0.1f));
		scoreLabel.GrowHorizontal = Control.GrowDirection.Both;
		DynamicFontData dynamicFontData = new DynamicFontData();
		dynamicFontData.FontPath = "res://Spartan-MB/SpartanMB-Regular.otf";
		DynamicFont defaultFont = new DynamicFont();
		defaultFont.FontData = dynamicFontData;
		defaultFont.Size = 28;
		Theme theme = new Theme();
		theme.DefaultFont = defaultFont;
		scoreLabel.Theme = theme;
		scoreLabel.Name = SCORE_LABEL_NODE_NAME;
		scoreLabel.Text = getScoreText(currentScore);
		this.AddChild(scoreLabel);

		Label livesLabel = new Label();
		livesLabel.SetPosition(new Vector2(screenWidth / 2, screenHeight * 0.9f));
		livesLabel.GrowHorizontal = Control.GrowDirection.Both;
		livesLabel.Theme = theme;
		livesLabel.Name = LIVES_LABEL_NODE_NAME;
		livesLabel.Text = getLivesString(currentLives);
		this.AddChild(livesLabel);
	}

	/**
	 * Invoked at the beginning or when all players are dead
	 * Assumes that all one-time setup <see cref="setupGame" /> has already happened
	 */
	private void resetGame(float screenWidth, float screenHeight)
	{
		// Assuming player resources have been freed elsewhere...
		mousePlayer = null;
		deviceIdToPlayer.Clear();
		allPlayers.Clear();

		float centerX = screenWidth / 2;
		float centerY = screenHeight / 2;

		// For one player, just put the player in the center
		if (globals.PlayerInputMappings.Count == 1)
		{
			PlayerInputMapping playerInputMapping = globals.PlayerInputMappings[0];
			Player player = createPlayer(centerX, centerY, playerInputMapping);

			AddChild(player);
			player.PlayerDamageCooldownTimer.Start();
			addPlayerToCurrentState(player);
		}
		else
		{
			// In order to prevent overlapping of multiple players, we can
			// imagine spawn points to be in a circle of a certain distance (so that players are not overlapping)
			float angleBetweenPlayers = 2 * (float)Math.PI / globals.PlayerInputMappings.Count;
			float distanceFromCenter = 25;
			for (int i = 0; i < globals.PlayerInputMappings.Count; i++)
			{
				PlayerInputMapping playerInputMapping = globals.PlayerInputMappings[i];
				float x = centerX + distanceFromCenter * (float)Math.Cos(angleBetweenPlayers * i);
				float y = centerY + distanceFromCenter * (float)Math.Sin(angleBetweenPlayers * i);
				Player player = createPlayer(x, y, playerInputMapping);
				player.CurrentColorIndex = i % Globals.COLORS.Count;
				AddChild(player);
				player.PlayerDamageCooldownTimer.Start();
				addPlayerToCurrentState(player);
			}
		}

		// Clear out any previous mobs
		Godot.Collections.Array children = this.GetChildren();
		foreach (object child in children)
		{
			if (child is Mob)
			{
				Mob mob = (Mob)child;
				mob.DestroyMob();
			}
		}

		currentNumMobs = 0;
		mobSpawnTimer.Start(MOB_SPAWN_TIME_S);
	}

	/**
	 * <param name="positionX">X-axis position relative to parent.</param>
	 * <param name="positionY">Y-axis position relative to parent.</param>
	 * <param name="playerInputMapping">Indicates the input to map to the new player to create</param>
	 * <returns>A new instance of a Player.  Caller is responsible for freeing said Player.</returns>
	 */
	private Player createPlayer(float positionX, float positionY, PlayerInputMapping playerInputMapping)
	{
		Player player = (Player)playerScene.Instance();
		player.Position = new Vector2(positionX, positionY);
		player.CurrPlayerInputMapping = playerInputMapping;
		player.Connect(nameof(Player.PlayerHitByMob), this, nameof(HandleOnPlayerDestroyed));
		return player;
	}

	private void addPlayerToCurrentState(Player player)
	{
		if (player.CurrPlayerInputMapping.IsMouse)
		{
			this.mousePlayer = player;
		}
		else
		{
			deviceIdToPlayer.Add(player.CurrPlayerInputMapping.DeviceId, player);
		}
		allPlayers.Add(player);
	}

	private void removePlayerFromCurrentState(Player player)
	{
		if (player.CurrPlayerInputMapping.IsMouse)
		{
			this.mousePlayer = null;
		}
		else
		{
			deviceIdToPlayer.Remove(player.CurrPlayerInputMapping.DeviceId);
		}
		allPlayers.Remove(player);
	}

	private void SpawnMob()
	{
		if (currentNumMobs >= maxNumMobs)
		{
			return;
		}

		GameContext gameContext = new GameContext(this.allPlayers);
		Mob createdMob;
		double val = random.NextDouble();
		if (val <= 0.2)
		{
			createdMob = CreateOctagonMob();
		}
		else if (val <= 0.4)
		{
			createdMob = CreateSquareMob(mobBorderSpawnFn);
		}
		else
		{
			createdMob = CreateTriangleMob(mobBorderSpawnFn);
		}
		createdMob.OnMobCreated(gameContext);
		currentNumMobs++;
	}

	private TriangleMob CreateTriangleMob(PathFollow2D mobSpawnFn)
	{
		TriangleMob mobInstance = (TriangleMob)mobScene.Instance();
		mobInstance.Position = GetNewBorderSpawnLoc(mobSpawnFn);
		mobInstance.Connect(nameof(TriangleMob.TriangleMobDestroyed), this, nameof(HandleMobDestroyed));
		mobInstance.Connect(nameof(TriangleMob.TriangleMobSelfDestructed), this, nameof(HandleMobSelfDestructed));
		mobInstance.Connect(nameof(TriangleMob.TriangleMobHit), this, nameof(HandleOnMobHit));
		AddChild(mobInstance);
		return mobInstance;
	}

	private SquareMob CreateSquareMob(PathFollow2D mobSpawnFn)
	{
		SquareMob squareMob = (SquareMob)squareMobScene.Instance();
		squareMob.Position = GetNewBorderSpawnLoc(mobSpawnFn);
		AddChild(squareMob); // Must first call ready for children to be connectable
		
		squareMob.connectOnSquareMobPortionHit(this, nameof(HandleOnMobHit));
		squareMob.Connect(nameof(SquareMob.SquareMobDestroyed), this, nameof(HandleSquareMobDestroyed));
		return squareMob;
	}

	private OctagonMob CreateOctagonMob()
	{
		OctagonMob octagonMob = (OctagonMob)octagonMobScene.Instance();
		// We need to give some padding so it doesn't spawn partially offscreen.
		// If we get a value from 0 to 1, this represents where on the range from [radius, x - radius]
		float positionX = (float)random.NextDouble() * (GetViewport().Size.x - 2 * octagonMob.getMaxRadius());
		positionX += octagonMob.getMaxRadius();

		float positionY = (float)random.NextDouble() * (GetViewport().Size.y - 2 * octagonMob.getMaxRadius());
		positionY += octagonMob.getMaxRadius();

		octagonMob.Position = new Vector2(positionX, positionY);

		octagonMob.Connect(nameof(OctagonMob.OctagonMobDestroyed), this, nameof(HandleMobDestroyed));
		octagonMob.Connect(nameof(OctagonMob.OctagonMobHit), this, nameof(HandleOnMobHit));

		this.AddChild(octagonMob);
		return octagonMob;
	}

	public void OnResize()
	{
		this.GetTree().CallGroup(RESIZABLE_GROUP_NAME, "OnMainSceneSizeChanged");
	}

	private void HandleOnMobHit(Node portion, Bullet bullet)
	{
		bullet.DestroyBullet();
	}

	// TODO: Can probably be made into common concept (probably breakdown into MobDestroyed and MobPortionDestroyed though
	// MobPortionDestroyed currently seems like a corner case)
	private void HandleSquareMobDestroyed(SquareMob squareMob)
	{
		// Can also use "HandleMobDestroyed", checking if bullet was already destroyed using Object.IsValidInstance(...)
		if (squareMob is Mob)
		{
			((Mob)squareMob).DestroyMob();
		}
		currentNumMobs--;

		currentScore++;
		this.GetNode<Label>(SCORE_LABEL_NODE_NAME).Text = getScoreText(currentScore);
	}

	private void HandleMobDestroyed(Node mob, Bullet bullet)
	{
		// Cleanup in this file, since these are created in this file as well
		if (mob is Mob)
		{
			((Mob)mob).DestroyMob();
		}
		bullet.DestroyBullet();
		currentNumMobs--;

		// Increment score
		currentScore++;
		this.GetNode<Label>(SCORE_LABEL_NODE_NAME).Text = getScoreText(currentScore);
	}

	private static String getScoreText(int score)
	{
		return "Score: " + score.ToString();
	}

	private void HandleMobSelfDestructed(Node mob)
	{
		if (mob is Mob)
		{
			((Mob)mob).DestroyMob();
		}
		currentNumMobs--;
	}

	private void HandleOnPlayerDestroyed(Player destroyedPlayer, Area2D playerDamager)
	{
		if (destroyedPlayer.IsPlayerDamageInCooldown)
		{
			return;
		}
		destroyedPlayer.IsPlayerDamageInCooldown = true;
		removePlayerFromCurrentState(destroyedPlayer);
		foreach (object node in this.GetChildren())
		{
			// TODO: this is a great indication that we should probably call a Mob interface method called "HandlePlayerDeath"
			// with the Player and Main as arguments, so that the Mob itself can modify its own internal functionality.
			// This is because only SquareMob needs to change the player.  Then we can also have a function called
			// "HandlePlayerCreation" in which the mob can change itself as well.
			if (node is Mob)
			{
				Mob mob = (Mob)node;
				mob.OnPlayerDestroyed(destroyedPlayer, new GameContext(allPlayers));
			}
		}
		destroyedPlayer.DestroyPlayer();

		currentNumMobs--;
		playerDamager.QueueFree();

		currentLives--;
		this.GetNode<Label>(LIVES_LABEL_NODE_NAME).Text = getLivesString(currentLives);

		if (currentLives == 0)
		{
			gameOver();
		}
		else if (allPlayers.Count == 0)
		{
			GD.Print("Resetting game");
			resetGame(GetViewport().Size.x, GetViewport().Size.y);
		}
	}

	private void gameOver()
	{
		mobSpawnTimer.Stop();
		this.GetNode<Label>(LIVES_LABEL_NODE_NAME).Text = "GAME OVER.  Press [Space] or (A) to restart.";
		isGameOver = true;
	}

	private static String getLivesString(int lives)
	{
		return "Lives: " + lives.ToString();
	}

	/**
	 * Handle bullet creation when mouse is clicked.
	 * Exists in Main because bullet should exist regardless of whether Player is alive or not
	 * (i.e. cannot be a child of Player).
	 */
	public override void _Input(InputEvent @event)
	{
		if (isGameOver && (
			(@event is InputEventKey keyButton && @event.IsPressed() && keyButton.Scancode == (int)KeyList.Space) ||
				(@event is InputEventJoypadButton joyButton && @event.IsPressed() && joyButton.ButtonIndex == (int)JoystickList.XboxA)
			)
		)
		{
			this.GetTree().ChangeScene("res://MainScene.tscn");
		}

		// For every input, we need to check the device from the event and move the corresponding player
		// Problem is we do not have a unique way to identify an input
		if (@event is InputEventMouseButton eventMouseButton && @event.IsPressed())
		{
			if (mousePlayer == null || mousePlayer.IsPlayerFireInCooldown)
			{
				return;
			}
			mousePlayer.IsPlayerFireInCooldown = true;
			Vector2 clickPosition = eventMouseButton.Position;	
			Vector2 playerPosition = mousePlayer.Position;
			Bullet bullet = createBullet(mousePlayer, (clickPosition - playerPosition).Normalized());
			this.AddChild(bullet);
			mousePlayer.PlayerFireCooldownTimer.Start();
		}
	}

	public override void _Process(float delta)
	{
		base._Process(delta);

		// Joystick handling is in _Process instead of _Input because Godot doesn't offer a way to
		// handle Inputs being "held".
		foreach (Player joystickPlayer in deviceIdToPlayer.Values)
		{
			if (joystickPlayer.IsPlayerFireInCooldown)
			{
				continue;
			}
			float x = Input.GetJoyAxis(joystickPlayer.CurrPlayerInputMapping.DeviceId, (int)JoystickList.Axis2); // right horizontal
			float y = Input.GetJoyAxis(joystickPlayer.CurrPlayerInputMapping.DeviceId, (int)JoystickList.Axis3); // right vertical
			Vector2 direction = new Vector2(x, y);

			// ensure that the joystick is pulled a significant distance away from the center (to remove noise)
			// sqrt(2) is the longest distance
			if (direction.Length() < 0.7)
			{
				continue;
			}
			joystickPlayer.IsPlayerFireInCooldown = true;
			Vector2 normalizedDirection = direction.Normalized();
			Bullet bullet = createBullet(joystickPlayer, normalizedDirection);
			joystickPlayer.PlayerFireCooldownTimer.Start();
			this.AddChild(bullet);
		}
	}

	private static Bullet createBullet(Player firingPlayer, Vector2 normalizedDirectionOfFire)
	{
		Bullet bullet = (Bullet)bulletScene.Instance();
		bullet.NormalizedDirectionVector = normalizedDirectionOfFire;
		bullet.Position = firingPlayer.Position;
		bullet.CurrentColorIndex = firingPlayer.CurrentColorIndex;
		// Note that the camera is oriented this way: going left increases x
		// going down increases y.  Therefore, everything is actually FLIPPED from the normal
		// way of thinking.
		// With this in mind, because the bullet is currently pointing up,
		// we need to additionally rotate it clockwise 90 degrees to align with
		// x-Axis (which is pointing down).
		// Can also compute angle between Y axis or draw bullet shape rotated on X axis
		// (can't rotate here since we're setting rotation)
		bullet.Rotation = normalizedDirectionOfFire.Angle() + ((float)Math.PI / 2.0f);
		return bullet;
	}

	[System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.Synchronized)]
	private Vector2 GetNewBorderSpawnLoc(PathFollow2D mobSpawnFn)
	{
		// Ideally, the mob itself would determine the location it should spawn.  However, this is difficult
		// to do because spawning on a line is perfect for Path2D...but a Path2D needs to be added to the SceneTree
		// in order to use it.  The Path2D makes way more sense in the parent (which is made apparent if adding
		// the the Path2D to the mob itself...you'll see the Path2D move with the mob in debugging mode).
		// Additionally, we don't want many children referencing and mutating the path in parallel.
		// Consequently, it seems like the PathFollow2D abstraction is simply broken (there should be a stateless way
		// to define a set of points and randomly sample a point from that).
		mobSpawnFn.Offset = random.Next();
		return mobSpawnFn.Position;
	}
}

/**
 * Contains the interface needed for all "Mobs."  Mobs are enemies of the <see cref="Player"/>
 * and the goal for the Player is to destroy as many mobs as possible.
 */
public interface Mob
{
	/**
	 * Indicate mob should be destroyed and its resources removed.
	 */
	void DestroyMob();

	/**
	 * Handle any mob-specific logic when the game is created.
	 * At this point, the Mob has been added to the scene tree.
	 */
	void OnMobCreated(GameContext gameContext);

	/**
	 * Handle any mob-specific logic when a player is destroyed.
	 * At this point, the player has not yet been freed, but it is no longer on the list of active players.
	 */
	void OnPlayerDestroyed(Player destroyedPlayer, GameContext gameContext);
}

/**
 * Data that can be passed around so that different game objects can perform business logic.
 * For example, pass around data a Mob can use to determine what to do next when a Player is destroyed.
 */
public class GameContext
{
	// TODO: Make immutable, but I first need to test if people need to download any extra frameworks
	// or stuff if I use NuGet to get System.Collections.Immutables.
	private List<Player> allPlayers;

	/**
	 * <param name="allPlayers">A list of all the current players</param>
	 */
	public GameContext(List<Player> allPlayers)
	{
		this.allPlayers = allPlayers;
	}

	/**
	 * WARNING: DO NOT MUTATE.
	 * Return all the current players.  If a player was just destroyed, then the player will not appear in this list.
	 */
	public List<Player> GetAllPlayers()
	{
		return allPlayers;
	}
}

/**
 * Helper methods shared across multiple files.
 */
public class Utils
{

	public static Color BLACK = new Color(0, 0, 0, 1);

	public static Color WHITE = new Color(1, 1, 1, 1);

	/**
	 * Helper function to draw borders for polygons.
	 * <param name="node">The node to draw the border in.</param>
	 * <param name="vertices">The line drawn will be the lines connecting each vertex, in order.</param>
	 * <param name="borderColor">Color of the border.</param>
	 */
	public static void DrawBorder(Node2D node, Vector2[] vertices, Color borderColor)
	{
		for (int i = 0; i < vertices.Length; i++)
		{
			Vector2[] lineVertices = new Vector2[2];
			lineVertices[0] = vertices[i];
			lineVertices[1] = vertices[(i + 1) % vertices.Length];
			node.DrawPolyline(lineVertices, borderColor, width: 1.0f, antialiased: true);
		}
	}

	/**
	 * Replace the collision shape on re-draw.  Replacement instead of creation prevents memory leak
	 * where every redraw creates a new child.  Replacement is determined by existence of a node with
	 * the given name.
	 * 
	 * <param name="node">The node to add the collision shape to.</param>
	 * <param name="childNodeName">Name of the child, which will use to uniquely identify the child for "node"</param>
	 * <param name="replacementNode">Node to replace the previous child with.</param>
	 */
	public static void ReplaceCollisionChild(Node node, String childNodeName, Node replacementNode)
	{
		Node prevChild = null;
		if (node.HasNode(childNodeName))
		{
			prevChild = node.GetNode(childNodeName);
		}

		replacementNode.Name = childNodeName; // in case it wasn't already set by caller
		node.AddChild(replacementNode);

		if (prevChild != null)
		{
			// free after adding replacement to prevent issues where collision doesn't exist in between
			// replacements.
			node.RemoveChild(prevChild);
			prevChild.QueueFree();
		}
	}

	/**
	 * Generate a tuple that can be used to generate mob spawn locations at the edge of the screen.
	 */
	public static Tuple<Path2D, PathFollow2D> GenerateBorderSpawnLocations(float screenWidth, float screenHeight)
	{
		// x OR y offset (used to push the spawn curve out of bounds so mobs start outside of the screen
		// or push the spawn curve into bounds so we can see mobs inside the screen).
		// Negative pushes it out, positive pushes it in.
		const float spawnOffset = -50f;

		// Dynamically create so we can just spawn bounds on resize
		Path2D mobSpawnPath = new Path2D(); // Simply a container for a curve
		Curve2D mobSpawnCurve = new Curve2D(); // Curve will describe a line on which mobs can spawn
		mobSpawnCurve.BakeInterval = 5;
		Vector2 topLeft = new Vector2(0.0f + spawnOffset, 0.0f + spawnOffset);
		Vector2 topRight = new Vector2(screenWidth - spawnOffset, 0.0f + spawnOffset);
		Vector2 bottomRight = new Vector2(screenWidth - spawnOffset, screenHeight - spawnOffset);
		Vector2 bottomLeft = new Vector2(0.0f + spawnOffset, screenHeight - spawnOffset);
		mobSpawnCurve.AddPoint(topLeft);
		mobSpawnCurve.AddPoint(topRight);
		mobSpawnCurve.AddPoint(bottomRight);
		mobSpawnCurve.AddPoint(bottomLeft);
		mobSpawnCurve.AddPoint(topLeft);
		mobSpawnPath.Curve = mobSpawnCurve;
		PathFollow2D mobSpawnFn = new PathFollow2D();
		mobSpawnFn.Loop = true;
		mobSpawnFn.CubicInterp = true;
		mobSpawnFn.Rotate = true;
		mobSpawnFn.Lookahead = 4.0f;
		mobSpawnPath.AddChild(mobSpawnFn); // TODO: cleanup on resize

		return Tuple.Create(mobSpawnPath, mobSpawnFn);
	}


	/**
	 * <param name="mob">A mob, which may or may not already been in the SceneTree.  The mob must have a valid position.</param> 
	 * <returns>null if there are no players on the map at the moment.  Otherwise, the player nearest to the given mob</returns>
	 */
	public static Player GetNearestPlayer(Node2D mob, List<Player> allPlayers)
	{
		Player nearestPlayer = null;
		float nearestDistance = float.MaxValue;

		foreach (Player player in allPlayers)
		{
			float distanceTo = player.Position.DistanceSquaredTo(mob.Position);
			if (distanceTo < nearestDistance)
			{
				nearestDistance = distanceTo;
				nearestPlayer = player;
			}
		}
		return nearestPlayer;
	}
}

/**
 * Indicates that the implementing object can damage the player if it collides with player.
 */
public interface PlayerDamager
{

}
