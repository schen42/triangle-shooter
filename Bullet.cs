using Godot;
using System;

/**
 * <summary>
 * Represents a bullet that is fired by a <see cref="Player"/>.  Bullets should match the color the player was when it was fired.
 * Bullets of a color can damage/destroy <see cref="Mob">Mobs</see> of the same color.  Mob-specific bullets are handled in other files.
 * </summary>
 * 
 * From experimentation, <see cref="KinematicBody2D"/> must be used over <see cref="StaticBody2D"/>
 * since collisions with <see cref="Area2D"/> is not detected unless the <see cref="Area2D"/> is moving.
 * Area2D is not used since it doesn't seem to (abstractly) fit the bill.
 * Potential starting point: https://godotengine.org/qa/15115/body_enter-emitted-passes-through-area2d-finishes-behind
 * 
 * A bullet can be represented as a circle (bullet head) and rectangle (bullet body) (where part of the circle overlaps with the rectangle to create a semi-circle bullet head).
 */
public class Bullet : KinematicBody2D
{
	/**
	 * Indicates how fast the bullet should move in pixels per second.
	 */
	private const float BULLET_SPEED_PX_PER_S = 550;

	/**
	 * The x-axis position of the center of the player, relative to anchor.
	 * Mainly used for debugging (i.e. parent anchor is 0,0 so if we want the whole object to be within the screen
	 * for quick inspection, we can make this a positive value).
	 */
	private float centerX = 0;

	/**
	 * The y-axis position of the center of the player, relative to anchor.
	 * Mainly used for debugging (i.e. parent anchor is 0,0 so if we want the whole object to be within the screen
	 * for quick inspection, we can make this a positive value).
	 */
	private float centerY = 0;

	/**
	 * The radius of the circular part (bullet head) of the bullet in pixels.
	 */
	private float bulletRadiusPx = 5;

	/**
	 * The height of the rectangular part of the bullet (bullet body) in pixels (the width is 2x the radius of the bullet head).
	 */
	private float bulletHeightPx = 10;

	/**
	 * Variable that stores the current screensize.  Just imitating the tutorial and is probably a
	 * premature optimization (could probably be easier just to call <see cref="Node.GetViewport"/>).
	 * Just copying Godot documentation here.
	 */
	private Vector2 screenSize;

	/**
	 * REQUIRED DURING INSTANTIATION.  Indicates the direction the bullet should be facing when instantiated.
	 * Must be normalized using a method like <see cref="Vector2.Normalized"/>.
	 */
	public Vector2 NormalizedDirectionVector { get; set; }

	/**
	 * REQUIRED DURING INSTANTIATION.  The color of the current bullet, which is an index into <see cref="Globals.COLORS"/>.
	 */
	public int CurrentColorIndex
	{
		get;
		set;
	} = 0;

	public override void _Ready()
	{
		screenSize = GetViewport().Size;
	}

	public override void _Draw()
	{
		base._Draw();

		Vector2 center = new Vector2(centerX, centerY);
		Rect2 bulletBaseRect = new Rect2(
			// need to offset x since position is relative to top right of rect
			centerX - bulletRadiusPx,
			centerY,
			bulletRadiusPx * 2,
			bulletHeightPx
		);

		Color currentColor = Globals.COLORS[CurrentColorIndex];
		this.DrawCircle(center, bulletRadiusPx, currentColor); // bullet head
		this.DrawRect(bulletBaseRect, currentColor, filled: true); // bullet base

		// Outline
		this.DrawArc(center, bulletRadiusPx, 0, -(float)Math.PI, 1000, Utils.BLACK, width: 1); // bullet head
		this.DrawRect(bulletBaseRect, Utils.BLACK, filled: false); // bullet base

		// Collision
		CollisionShape2D bulletHeadShape = new CollisionShape2D();
		// Because no offset ability, position node instead
		bulletHeadShape.Position += new Vector2(centerX, centerY);
		CircleShape2D bulletHeadCircle = new CircleShape2D();
		bulletHeadCircle.Radius = bulletRadiusPx;
		bulletHeadShape.Shape = bulletHeadCircle;
		Utils.ReplaceCollisionChild(this, "HEAD_COLLISION_SHAPE", bulletHeadShape);

		Vector2[] bulletBodyPolygonPoints = {
			new Vector2(centerX - bulletRadiusPx, centerY), // top left
			new Vector2(centerX + bulletRadiusPx, centerY), // top right
			new Vector2(centerX + bulletRadiusPx, centerY + bulletHeightPx), // bottom right
			new Vector2(centerX - bulletRadiusPx, centerY + bulletHeightPx), // bottom left
		};
		ConvexPolygonShape2D bulletBodyPolygon = new ConvexPolygonShape2D();
		bulletBodyPolygon.Points = bulletBodyPolygonPoints;
		
		CollisionShape2D bulletBodyCollisionShape = new CollisionShape2D();
		bulletBodyCollisionShape.Shape = bulletBodyPolygon;
		Utils.ReplaceCollisionChild(this, "BODY_COLLISION_SHAPE", bulletBodyCollisionShape);
	}

	public override void _Process(float delta)
	{
		base._Process(delta);

		if (0 <= this.Position.x && this.Position.x <= screenSize.x && 0 <= this.Position.y && this.Position.y <= screenSize.y)
		{
			this.Position += (this.NormalizedDirectionVector * BULLET_SPEED_PX_PER_S * delta);
		} else
		{
			// Out of bounds, so destroy (TODO: Destroy in main by sending BulletSelfDestruct signal)
			this.QueueFree();
		}
	}

	/**
	 * Cleanup bullet resources.  Other classes instantiate bullet and should be also responsible for cleaning it up.
	 */
	public void DestroyBullet()
	{
		this.QueueFree();
	}
}
