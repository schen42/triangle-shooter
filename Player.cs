using Godot;
using System;

/**
 * <summary>
 * The player, which is currently represented as a circle.
 * Players can fire bullets and cycle colors (they can only damage enemies of matching colors).
 * </summary>
 * 
 * Other classes are expected to handle:
 * - Player damage (since this involves cross-component interaction.  For example, other classes may want to control
 *   when a player can be damaged or not since they control the damage logic like life loss).
 * - Player firing (since this involves cross-component interaction.  For example, bullet should remain in play
 *   even after a player is destroyed).
 * 
 * <remarks>
 * There's an example of resizabiltiy here that is not in other files (just to show how resizing can be done).
 * It is otherwise unused.
 * </remarks>
 */
public class Player : Area2D
{
	/**
	 * Number of pixels to move per second.
	 *
	 * [Export] can be used to show the property as a configurable setting in the UI.  Here's an example.
	 */
	[Export]
	private int Speed = 400;

	/**
	 * The radius of the player, which is a circle.
	 */
	public const int PLAYER_RADIUS_PX = 20;

	/**
	 * The approximate amount of time (in seconds) after which a player has been damaged/destroyed, and cannot
	 * be damaged/destroyed again.  Useful for scenarios like player respawn (not a current feature),
	 * but mainly only used during debugging (when we speed up spawning, rounds, etc. for faster end-to-end tests)
	 */
	private const int PLAYER_DAMAGE_COOLDOWN_S = 2;

	/**
	 * The approximate amount of time (in seconds) after which a player has fired a bullet, and cannot
	 * fire a bullet again. Used to prevent spamming of bullets.
	 */
	private const float PLAYER_FIRE_COOLDOWN_S = 0.5f;

	/**
	 * The approximate amount of time (in seconds) after which a player has cycled colors, and cannot
	 * cycle colors again again. Used to prevent spamming of color changes.
	 */
	private const float PLAYER_CYCLE_COLOR_COOLDOWN_S = 0.5f;

	/**
	 * Constant to indicate that we should cycle color forward (think of the colors as a circular list).
	 * If false, that means we cycle backwards.
	 */
	private const bool IS_CYCLE_COLOR_FORWARD = true;

	/**
	 * Here's my current thinking:  If players can toggle color, then they can continue playing while other players
	 * are gone.  OR, we could add to the challenge by making it so they can't destroy the remaining mobs and see how
	 * long they live.
	 */
	private Boolean CanPlayerToggleColor = true;

	/**
	 * Variable that stores the current screensize.  Just imitating the tutorial and is probably a
	 * premature optimization (could probably be easier just to call <see cref="Node.GetViewport"/>).
	 * Just copying Godot documentation here.
	 */
	private Vector2 screenSize; // Size of the game window.

	/**
	 * The x-axis position of the center of the player, relative to anchor.
	 * Mainly used for debugging (i.e. parent anchor is 0,0 so if we want the whole object to be within the screen
	 * for quick inspection, we can make this a positive value).
	 */
	private float centerX = 0;

	/**
	 * The y-axis position of the center of the player, relative to anchor.
	 * Mainly used for debugging (i.e. parent anchor is 0,0 so if we want the whole object to be within the screen
	 * for quick inspection, we can make this a positive value).
	 */
	private float centerY = 0;

	/**
	 * REQUIRED DURING INSTANTIATION.  The color of the current player, which is an index into <see cref="Globals.COLORS"/>.
	 */
	public int CurrentColorIndex
	{
		get;
		set;
	} = 0;

	/**
	 * Indicate if we are not allowed to change colors because it was recently changed.
	 */
	private bool isColorCyclingCooldown = false;

	/**
	 * Timer that will be used to determine when a player is allowed to cycle colors again.
	 */
	private Timer colorCyclingCooldownTimer;

	/**
	 * Timer that should be used to determine when a player is allowed to take damage again.
	 * Other classes are expected to trigger the cooldown.
	 */
	public Timer PlayerDamageCooldownTimer
	{
		get;
		private set;
	}

	/**
	 * Timer that should be used to determine when a player is allowed to fire again.
	 * Other classes are expected to trigger the cooldown.
	 */
	public Timer PlayerFireCooldownTimer
	{
		get;
		private set;
	}

	/**
	 * REQUIRED DURING INSTANTIATION. The <see cref="PlayerInputMapping"/> assigned to this player.
	 */
	public PlayerInputMapping CurrPlayerInputMapping
	{
		get;
		set;
	}

	/**
	 * Used to indicate that player is out of damage cooldown.
	 * Other classes are expected to trigger the cooldown.
	 */
	public bool IsPlayerDamageInCooldown = true;

	/**
	 * Used to indicate that player is out of damage cooldown.
	 * Other classes are expected to trigger the cooldow.
	 */
	public bool IsPlayerFireInCooldown = false;

	/**
	 * Signal that indicates that a specific player was hit by a specific <see cref="PlayerDamager"/>
	 * (Godot only allows Node parameters in signals).
	 */
	[Signal]
	public delegate void PlayerHitByMob(Player player, Area2D playerDamager);

	public override void _Ready()
	{
		screenSize = GetViewport().Size;
		AddToGroup(Main.RESIZABLE_GROUP_NAME);

		PlayerDamageCooldownTimer = new Timer();
		PlayerDamageCooldownTimer.OneShot = true;
		PlayerDamageCooldownTimer.Autostart = false;
		PlayerDamageCooldownTimer.WaitTime = PLAYER_DAMAGE_COOLDOWN_S;
		PlayerDamageCooldownTimer.Connect("timeout", this, nameof(Player.EndPlayerDamageCooldown));
		this.AddChild(PlayerDamageCooldownTimer);
		PlayerDamageCooldownTimer.Start();

		PlayerFireCooldownTimer = new Timer();
		PlayerFireCooldownTimer.OneShot = true;
		PlayerFireCooldownTimer.Autostart = false;
		PlayerFireCooldownTimer.WaitTime = PLAYER_FIRE_COOLDOWN_S;
		PlayerFireCooldownTimer.Connect("timeout", this, nameof(Player.EndPlayerFireCooldown));
		this.AddChild(PlayerFireCooldownTimer);
		PlayerFireCooldownTimer.Start();

		colorCyclingCooldownTimer = new Timer();
		colorCyclingCooldownTimer.WaitTime = PLAYER_CYCLE_COLOR_COOLDOWN_S;
		colorCyclingCooldownTimer.OneShot = true;
		colorCyclingCooldownTimer.Autostart = false;
		colorCyclingCooldownTimer.Connect("timeout", this, nameof(unsetIsColorCycling));
		this.AddChild(colorCyclingCooldownTimer);

		this.Connect("area_entered", this, nameof(OnPlayerAreaEntered));
	}

	/**
	 * Execute logic when player is "hit" by something.
	 */
	public void OnPlayerAreaEntered(Area2D area)
	{
		// Separate timer is needed to debounce how often a player can be "hit" (handled outside of Player).
		if (area is PlayerDamager)
		{
			this.EmitSignal(nameof(PlayerHitByMob), new object[] { this, area });
		}
	}

	public override void _Draw()
	{
		base._Draw();
		float radius = PLAYER_RADIUS_PX;
		Color playerStartingColor = Globals.COLORS[CurrentColorIndex];
		this.DrawCircle(new Vector2(centerX, centerY), radius, playerStartingColor);

		CircleShape2D circleShape2D = new CircleShape2D();
		circleShape2D.Radius = radius;

		CollisionShape2D collisionShape2D = new CollisionShape2D();
		collisionShape2D.Position = new Vector2(centerX, centerY);
		collisionShape2D.Shape = circleShape2D;
		Utils.ReplaceCollisionChild(this, "COLLISION_SHAPE", collisionShape2D);
	}

	/**
	 * See <see cref="IsPlayerDamageInCooldown"/>.
	 */
	private void EndPlayerDamageCooldown()
	{
		IsPlayerDamageInCooldown = false;
	}

	/**
	 *  See <see cref="IsPlayerFireInCooldown"/>.
	 */
	private void EndPlayerFireCooldown()
	{
		IsPlayerFireInCooldown = false;
	}

	public override void _Process(float delta)
	{
		var velocity = new Vector2();
		if (CurrPlayerInputMapping.IsMouse)
		{
			// Constants are defined in InputMap
			if (Input.IsActionPressed("ui_right"))
			{
				velocity.x += 1;
			}

			if (Input.IsActionPressed("ui_left"))
			{
				velocity.x -= 1;
			}

			if (Input.IsActionPressed("ui_down"))
			{
				velocity.y += 1;
			}

			if (Input.IsActionPressed("ui_up"))
			{
				velocity.y -= 1;
			}
		}
		else
		{
			float x = Input.GetJoyAxis(CurrPlayerInputMapping.DeviceId, (int)JoystickList.Axis0); // left horizontal
			float y = Input.GetJoyAxis(CurrPlayerInputMapping.DeviceId, (int)JoystickList.Axis1); // left vertical
			Vector2 direction = new Vector2(x, y);
			// Account for stick drift by ensuring that only "strong" joystick directionality
			// is considered for movement. This value was manually determined by feel.
			// See: 
			if (direction.Length() > 0.7)
			{
				velocity.x += x;
				velocity.y += y;
			}
		}

		// Premature optimization
		if (velocity.Length() > 0)
		{
			velocity = velocity.Normalized() * Speed;
			Position = GetVectorClampedIntoScreenBounds(Position + velocity * delta);
		}
	}

	/**
	 * Note that movement is handled in <see cref="_Process"/> and not <see cref="_Input"/>.
	 * This is to account for the joystick, which only triggers an _Input event on the first press (this is a
	 * problem because we want a player to move when they're holding down the joystick).
	 */
	public override void _Input(InputEvent @event)
	{
		base._Input(@event);

		if (@event is InputEventKey keyboardButton && keyboardButton.IsPressed())
		{
			if (CanPlayerToggleColor && !isColorCyclingCooldown && keyboardButton.IsActionPressed("ui_cycle_color_left"))
			{
				cycleColors(IS_CYCLE_COLOR_FORWARD);
			}
			else if (CanPlayerToggleColor && !isColorCyclingCooldown && keyboardButton.IsActionPressed("ui_cycle_color_right"))
			{
				cycleColors(!IS_CYCLE_COLOR_FORWARD);
			}
		}
		else if (@event is InputEventJoypadButton joyButton && joyButton.Device == CurrPlayerInputMapping.DeviceId && joyButton.IsPressed())
		{
			if (CanPlayerToggleColor && !isColorCyclingCooldown && joyButton.ButtonIndex == (int)JoystickList.R)
			{
				cycleColors(IS_CYCLE_COLOR_FORWARD);
			}
			else if (CanPlayerToggleColor && !isColorCyclingCooldown && joyButton.ButtonIndex == (int)JoystickList.L)
			{
				cycleColors(!IS_CYCLE_COLOR_FORWARD);
			}
		}
	}

	private void cycleColors(bool isCycleForward)
	{
		isColorCyclingCooldown = true;
		CurrentColorIndex = (CurrentColorIndex + (isCycleForward ? 1 : -1)) % Globals.COLORS.Count;
		if (CurrentColorIndex < 0) // Possible because we're subtracting and positive# % negative# = negative#
		{
			CurrentColorIndex += Globals.COLORS.Count;
		}
		this.Update();
		colorCyclingCooldownTimer.Start();
	}

	/**
	 * See <see cref="isColorCyclingCooldown"/>.
	 */
	private void unsetIsColorCycling()
	{
		isColorCyclingCooldown = false;
	}

	/**
	 * Free the player resources.  Other methods handle Player instantiation, so they are also expected to handle
	 * Player cleanup.
	 */
	public void DestroyPlayer()
	{
		this.QueueFree();
	}
	
	private void OnMainSceneSizeChanged()
	{
		screenSize = GetViewport().Size;
		// The better way to do this is to scale...maybe find another way to do it.
		GetVectorClampedIntoScreenBounds(this.Position);
		this.Update();
	}

	private Vector2 GetVectorClampedIntoScreenBounds(Vector2 vector) {
		return new Vector2(
			x: Mathf.Clamp(vector.x, 0, screenSize.x),
			y: Mathf.Clamp(vector.y, 0, screenSize.y)
		);
	}
}
