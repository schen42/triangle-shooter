# Summary

I started this project to get a simple introduction into game development.

A few caveats
- The game itself is not polished or complete.  There are a lot of features to add still that I'll eventually get to (see TODOs).
- The code is somewhat of a mess, caused by a mixture of having forgotten C#, not understanding the "standard practices" in Godot, and just timeboxing the amount of time I would spend on this.  Styles are also mixed due to not having an established style guide (currently optimizing learning Godot over C#).
- It is probably buggy. Multiplayer in particular is hard to test with actual players during COVID-19. Can add unit testing after coding stabilizes (currently optimizing for speed over correctness).

# How to play

Download the .pck and the .exe file.  Click the .exe file.  Only Windows is supported right now.

# TODO

- Pause button
- Test-drive local multiplayer

- Countdown timer when game starts and in-between deaths
- Save high score
- Remove InputMapping
- Somehow make Mob interface define signals the same way (so score can go up or bullet can be destroyed)
- Have TriangleMob spawn on line (keep in mind that OctagonMob will not spawn on line but anywhere on the map.  I think this will involve defining the path within the mob itself)
- new enemy types (fast rushers, slow movers)
	- white enemy that takes a lot of damage from all color sources but doesn't move
- new weapons
- Configurable buttons
- Tie buttons to all the prompts.
- (?) Refactor/re-organize code to pass in Main so that Main doesn't need to know how to instantiate code?
- Music
- (?) Unit tests.  Some things can easily be unit tested (conditions in code).  It will be more useful to test some things with integration/end-to-end tests.  Finally, some things will just be too difficult to test without actual playtesting (the "feeling" of difficulty, speed, etc.).
- (?) Adopt C# style standard.

# User feedback
- Too hard
- Why are lives combined?  
