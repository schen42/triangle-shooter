using Godot;
using System.Collections.Generic;
using System;

/**
 * <summary>
 * This class is intended to store values used across multiple files, singletons, etc.
 * </summary>
 *
 * This class is manually attached as an Autoload singleton:
 * https://docs.godotengine.org/en/stable/getting_started/step_by_step/singletons_autoload.html
 * 
 * Can either use globals: https://godotengine.org/qa/1883/transfering-a-variable-over-to-another-scene
 * or have a Node that loads scenes.  Globals seems easier.
 */
public class Globals : Node
{
	/**
	 * Access is not thread-safe.
	 * Remember to use <see cref="playerInputMappingsLock"/> or other lock when mutating this object.
	 */
	public List<PlayerInputMapping> PlayerInputMappings
	{
		get;
	} = new List<PlayerInputMapping>();

	/**
	 * Lock for <see cref="playerInputMappings"/>
	 */
	private readonly object PlayerInputMappingsLock = new object();

	public override void _Ready()
	{
		
	}

	/**
	 * Atomically add a <see cref="PlayerInputMapping"/> to the global list.
	 */
	public void AddPlayerInputMapping(PlayerInputMapping playerInputMapping)
	{
		lock(PlayerInputMappingsLock)
		{
			PlayerInputMappings.Add(playerInputMapping);
		}
	}

	/**
	 * Convenience method to load the Globals scene.
	 */
	public static Globals LoadGlobals(Node nodeToLoadInto)
	{
		// Is set through UI (Autoload Singleton)
		return nodeToLoadInto.GetNode<Globals>("/root/Globals");
	}

	/**
	 * The supported colors in the game, to be applied to mobs, players, bullets, etc.
	 * 
	 * Keep a reference to the color by the index in this list to decouple the actual color from what the
	 * color the game object should be.
	 * 
	 * In the future, we should:
	 * - Allow this to be adjustable in-game to account for color vision problems
	 * - Dynamically add colors based on the number of players (while also picking the "most different" colors by default)
	 */
	public static List<Color> COLORS = new List<Color>()
	{
		new Color(1, 0.4f, 0.4f),
		new Color(0.4f, 1, 0.4f),
		new Color(0.4f, 0.4f, 1),
		new Color(1, 0.4f, 1)
	};
}

/**
 * Keeps track of the input system for the player.
 */
public class PlayerInputMapping
{
	private const bool IS_MOUSE = true;

	/**
	 * This is the device ID passed by the Godot <see cref="InputEvent"/>.
	 * From testing, the deviceID sent by Godot is not unique (keyboard+mouse is always 0) and then
	 * the first input joystick can also be 0.  Use it with <see cref="IsMouse"/> to uniquely identify
	 * a controller.
	 */
	public int DeviceId
	{
		get;
	}

	/**
	 * If true, then player input is a mouse.  Otherwise, player input is a controller
	 */
	public bool IsMouse
	{
		get;
	}


	private PlayerInputMapping(int deviceId, bool isMouse)
	{
		this.DeviceId = deviceId;
		this.IsMouse = isMouse;
	}

	/**
	 * Convenience method to construct a <see cref="PlayerInputMapping"/>.
	 * <param name="deviceId">Can be retrieved from <see cref="InputEvent.Device"/></param>
	 */
	public static PlayerInputMapping CreateKeyboardMouseInputMapping(int deviceId)
	{
		return new PlayerInputMapping(deviceId, IS_MOUSE);
	}

	/**
	 * Convenience method to construct a <see cref="PlayerInputMapping"/>.
	 * <param name="deviceId">Can be retrieved from <see cref="InputEvent.Device"/></param>
	 */
	public static PlayerInputMapping CreateJoyInputMapping(int deviceId)
	{
		return new PlayerInputMapping(deviceId, !IS_MOUSE);
	}
}
