using Godot;
using System;

/**
 * <summary>
 * Triangle shoots towards the first nearest player and never changes course.
 * 
 * The parts can only be destroyed by a <see cref="Bullet"/> of the same color.
 * </summary>
 */
public class TriangleMob : Area2D, Mob, PlayerDamager
{
	private readonly Random random = new Random();

	/**
	 * Indicates how fast the mob should move in pixels per second.
	 */
	private const int TRAVEL_PIXELS_PER_S = 300;

	/**
	 * Mob is isosceles.  This half of the length of the non-equal side.
	 */
	private float triangleMobLength = 20;

	/**
	 * The color that can destroy this mob, which is an index
	 * into <see cref="Globals.COLORS"/>.  Default value for safety.
	 */
	private int currentColorIndex = 0;

	/**
	 * Indicates the direction the mob should be facing when instantiated.
	 * Must be normalized using a method like <see cref="Vector2.Normalized"/>.
	 * Default value for safety.
	 */
	private Vector2 normalizedDirection = new Vector2(1, 0);

	/**
	 * Keep track of distance traveled so it can be removed once it is off screen.
	 */
	private float distanceTraveled;

	/**
	 * Variable that stores the current screensize.  Just imitating the tutorial and is probably a
	 * premature optimization (could probably be easier just to call <see cref="Node.GetViewport"/>).
	 * Just copying Godot documentation here.
	 */
	private Vector2 screenSize;

	/**
	 * The x-axis position of the center of the player, relative to anchor.
	 * Mainly used for debugging (i.e. parent anchor is 0,0 so if we want the whole object to be within the screen
	 * for quick inspection, we can make this a positive value).
	 */
	private float centerX = 0;

	/**
	 * The y-axis position of the center of the player, relative to anchor.
	 * Mainly used for debugging (i.e. parent anchor is 0,0 so if we want the whole object to be within the screen
	 * for quick inspection, we can make this a positive value).
	 */
	private float centerY = 0;

	/**
	 * Indicates mob self-destructed (e.g. went out of bounds).  Provides a reference to the mob that self-destructed.
	 */
	[Signal]
	public delegate void TriangleMobSelfDestructed(Node mob);

	/**
	 * Indicates mob was destroyed.
	 */
	[Signal]
	public delegate void TriangleMobDestroyed();

	/**
	 * Indicates mob was hit by a bullet.  Different than <see cref="TriangleMobDestroyed"/> because it is possible
	 * to hit the mob without destroying it if the colors don't match.
	 * 
	 * Provides a reference to the mob that was hit and the bullet that hit the mob.
	 */
	[Signal]
	public delegate void TriangleMobHit(Node mob, Bullet bullet);

	public override void _Ready()
	{
		this.screenSize = GetViewport().Size;

		currentColorIndex = random.Next() % Globals.COLORS.Count;
		this.Connect("body_entered", this, nameof(OnMobBodyEntered));
	}

	public override void _Draw()
	{
		base._Draw();

		Vector2 topLeft = new Vector2(centerX, centerY - triangleMobLength);
		Vector2 right = new Vector2(centerX + (triangleMobLength * (float)Math.Sqrt(3)), centerY);
		Vector2 bottomLeft = new Vector2(centerX, centerY + triangleMobLength);

		Color[] fillColor = new Color[] { Globals.COLORS[currentColorIndex] };
		Vector2[] triangleVertices = { right, topLeft, bottomLeft };
		this.DrawPolygon(triangleVertices, fillColor);

		Utils.DrawBorder(this, triangleVertices, Utils.BLACK);

		ConvexPolygonShape2D convexPolygonShape2D = new ConvexPolygonShape2D();
		convexPolygonShape2D.Points = triangleVertices;
		CollisionShape2D collisionShape2D = new CollisionShape2D();
		collisionShape2D.Shape = convexPolygonShape2D;
		Utils.ReplaceCollisionChild(this, "COLLISION_SHAPE", collisionShape2D);
	}

	public override void _Process(float delta)
	{
		base._Process(delta);

		// Go in the direction of the first player seen and don't change directions
		if (normalizedDirection != null)
		{
			Vector2 distanceToTravel = (normalizedDirection * delta * TRAVEL_PIXELS_PER_S);
			distanceTraveled += distanceToTravel.Length();
			this.Position = this.Position += (normalizedDirection * delta * TRAVEL_PIXELS_PER_S);
		}
		else
		{
			GD.Print("WARNING: No first player position");
			// Still force caller to destroy for (a) consistency (b) someone else is handling mob count, etc.
			this.EmitSignal(nameof(TriangleMobSelfDestructed), new object[] { this });
			this.SetProcess(false);
		}

		if (distanceTraveled > screenSize.x + screenSize.y)
		{
			this.EmitSignal(nameof(TriangleMobSelfDestructed), new object[] { this });
			this.SetProcess(false);
		}
	}
	public void DestroyMob()
	{
		this.QueueFree();
	}

	private void OnMobBodyEntered(Node body)
	{
		if (body.GetType() == typeof(Bullet))
		{
			Bullet bullet = (Bullet)body;
			if (bullet.CurrentColorIndex == currentColorIndex)
			{
				this.EmitSignal(nameof(TriangleMobDestroyed), new object[] { this, bullet });
			} else
			{
				this.EmitSignal(nameof(TriangleMobHit), new object[] { this, bullet });
			}
		}
	}

	/**
	 * <inheritdoc/>
	 */
	void Mob.OnMobCreated(GameContext gameContext)
	{
		Player nearestPlayer = Utils.GetNearestPlayer(this, gameContext.GetAllPlayers());
		this.normalizedDirection = (nearestPlayer.Position - this.Position).Normalized();
		this.Rotation = normalizedDirection.Angle();
	}

	/**
	 * <inheritdoc/>
	 */
	void Mob.OnPlayerDestroyed(Player destroyedPlayer, GameContext gameContext)
	{
	}
}
