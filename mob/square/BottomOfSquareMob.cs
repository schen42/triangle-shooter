using Godot;
using System;

/**
 * <summary>
 * Bottom portion of <see cref="SquareMob"/>.  Separate object so that it can be destroyed separately.
 * </summary>
 */
public class BottomOfSquareMob : Area2D, SquareMob.SquarePortion, PlayerDamager
{
	private readonly Random random = new Random();

	/**
	 * Indicates that this piece was hit by a <see cref="Bullet"/>,
	 * including the piece that was hit and the bullet that hit it.
	 */
	[Signal]
	public delegate void BottomOfSquareMobHit(Node bottomOfSquareMob, Bullet bullet);

	/**
	 * REQUIRED DURING INSTANTIATION.  The color that can destroy this mob, which is an index
	 * into <see cref="Globals.COLORS"/>.
	 */
	private int currentColorIndex = 0;

	public override void _Ready()
	{
		currentColorIndex = random.Next() % Globals.COLORS.Count;
		this.Connect("body_entered", this, nameof(OnBodyEnteredTopSquarePortion));
	}

	/**
	 * Handle collision with this object.
	 */
	private void OnBodyEnteredTopSquarePortion(Node body)
	{
		if (body.GetType() == typeof(Bullet))
		{
			Bullet bullet = (Bullet)body;
			this.EmitSignal(nameof(BottomOfSquareMobHit), new object[] { this, bullet });
		}
	}

	public override void _Draw()
	{
		base._Draw();

		float squareMobOffset = SquareMob.squareMobSize / 2;
		Vector2 topLeft = new Vector2(SquareMob.centerX - squareMobOffset, SquareMob.centerY - squareMobOffset);
		Vector2 bottomLeft = new Vector2(SquareMob.centerX - squareMobOffset, SquareMob.centerY + SquareMob.squareMobSize - squareMobOffset);
		Vector2 bottomRight = new Vector2(SquareMob.centerX + SquareMob.squareMobSize - squareMobOffset, SquareMob.centerY + SquareMob.squareMobSize - squareMobOffset);

		Color[] fillColorBottom = new Color[] { Globals.COLORS[this.currentColorIndex] };
		Vector2[] bottomTriangleVertices = { topLeft, bottomLeft, bottomRight };
		this.DrawPolygon(bottomTriangleVertices, fillColorBottom);

		Utils.DrawBorder(this, bottomTriangleVertices, Utils.BLACK);

		ConvexPolygonShape2D convexPolygonShape2D = new ConvexPolygonShape2D();
		convexPolygonShape2D.Points = new Vector2[] { topLeft, bottomLeft, bottomRight };
		CollisionShape2D collisionShape2D = new CollisionShape2D();
		collisionShape2D.Shape = convexPolygonShape2D;
		Utils.ReplaceCollisionChild(this, "COLLISION_SHAPE", collisionShape2D);
	}

	/**
	 * <inheritdoc/>
	 */
	public bool IsTop()
	{
		return false;
	}

	/**
	 * <inheritdoc/>
	 */
	public void DestroyPortion()
	{
		this.QueueFree();
	}

	/**
	 * <inheritdoc/>
	 */
	public int GetCurrentColorIndex()
	{
		return this.currentColorIndex;
	}
}
