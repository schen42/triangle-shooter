using Godot;
using System;

/**
 * <summary>
 * SquareMob is comprised of two "parts" of potentially different colors
 * (<see cref="BottomOfSquareMob"/> and <see cref="TopOfSquareMob"/>), each which must be destroyed
 * separately to in order for the entire mob  to be considered destroyed.  It can rotate, follow the first
 * nearest player.  Rotation adds some timing challenge, but also helps player destroy
 * it when player can't easily move position.
 * 
 * The parts can only be destroyed by a <see cref="Bullet"/> of the same color.
 * </summary>
 */
public class SquareMob : Node2D, Mob
{
	/**
	 * The x-axis position of the center of the player, relative to anchor.
	 * Mainly used for debugging (i.e. parent anchor is 0,0 so if we want the whole object to be within the screen
	 * for quick inspection, we can make this a positive value).
	 * Visible for access to <see cref="BottomOfSquareMob"/> and <see cref="TopOfSquareMob"/>.
	 */
	public static float centerX
	{
		get;
		private set;
	} = 0;

	/**
	 * The y-axis position of the center of the player, relative to anchor.
	 * Mainly used for debugging (i.e. parent anchor is 0,0 so if we want the whole object to be within the screen
	 * for quick inspection, we can make this a positive value).
	 * Visible for access to <see cref="BottomOfSquareMob"/> and <see cref="TopOfSquareMob"/>.
	 */
	public static float centerY
	{
		get;
		private set;
	} = 0;
	
	/**
	 * Side length of the square.
	 */
	public static float squareMobSize = 50;

	/**
	 * Indicates how fast the mob should move in pixels per second.
	 */
	private const float TRAVEL_PIXELS_PER_S = 200;

	/**
	 * Indicates how fast the mob should rotate in pixels per second.
	 */
	private const float ROTATE_RADS_PER_S = 2;

	/**
	 * Stores the reference to the "top" part of the mob.
	 */
	private TopOfSquareMob top;

	/**
	 * Stores the reference to the "bottom" part of the mob.
	 */
	private BottomOfSquareMob bottom;

	private bool topDestroyed = false;
	private bool bottomDestroyed = false;

	/**
	 * Keep track of the nearest player to follow.
	 */
	private Player nearestPlayer = null;

	/**
	 * Signal to indicate mob was destroyed with the reference to the mob that was destroyed.
	 */
	[Signal]
	public delegate void SquareMobDestroyed(SquareMob squareMob);

	/**
	 * This needs to be within <see cref="SquareMob"/> and not <see cref="TopOfSquareMob"/>
	 * or <see cref="BottomOfSquareMob"/> because.
	 * Also, the signal parameter must be a Godot.Object (otherwise, the signal will not work):
	 * https://github.com/godotengine/godot/issues/16706
	 */
	[Signal]
	public delegate void SquarePortionDestroyed(Node squarePortion, Bullet bullet);

	/**
	 * Called when the node enters the scene tree for the first time.
	 * Note that we do not dynamically create top and bottom Area2Ds here or in <see cref="CanvasItem._Draw" /> because we will not be able
	 * to draw the top and bottom into separate Area2D (which is needed if we want to destroy them separately />
	 */
	public override void _Ready()
	{
		// Nodes are created in Scene via UI and names are assigned in UI as well
		// Keep track of these for later deletion (we are considering the children of this object to be this object's reponsibility)
		top = this.GetNode<TopOfSquareMob>("TopOfSquareMob");
		bottom = this.GetNode<BottomOfSquareMob>("BottomOfSquareMob");
		
		top.Connect(nameof(TopOfSquareMob.TopOfSquareMobHit), this, nameof(EvaluateWholeBodyDestroyed));
		bottom.Connect(nameof(BottomOfSquareMob.BottomOfSquareMobHit), this, nameof(EvaluateWholeBodyDestroyed));
	}

	public override void _Process(float delta)
	{
		base._Process(delta);

		if (Godot.Object.IsInstanceValid(nearestPlayer) && nearestPlayer != null)
		{
			this.Position = this.Position.MoveToward(nearestPlayer.Position, delta * TRAVEL_PIXELS_PER_S);
			this.Rotate(delta * ROTATE_RADS_PER_S);
		}
	}

	/**
	 * Must be called after <see cref="_Ready"/>.
	 */
	public void connectOnSquareMobPortionHit(Godot.Object target, String method)
	{
		top.Connect(nameof(TopOfSquareMob.TopOfSquareMobHit), target, method);
		bottom.Connect(nameof(BottomOfSquareMob.BottomOfSquareMobHit), target, method);
	}

	/**
	 * Handle destruction of portion and emit signal that the mob is destroyed if both portions are destroyed.
	 */
	private void EvaluateWholeBodyDestroyed(Node squarePortionDestroyed, Bullet bullet)
	{
		// Godot doesn't support signal parameters that aren't Godot.Objects (such as interfaces such as
		// SquarePortion).  So pass a Node and cast it.
		SquareMob.SquarePortion squarePortion = (SquareMob.SquarePortion)squarePortionDestroyed;
		if (squarePortionDestroyed.GetType() == typeof(TopOfSquareMob) && bullet.CurrentColorIndex == squarePortion.GetCurrentColorIndex())
		{
			topDestroyed = true;
			top.QueueFree(); // Since portions are children of squareMob in UI, squareMob is responsible for destroying them
		} else if (squarePortionDestroyed.GetType() == typeof(BottomOfSquareMob) && bullet.CurrentColorIndex == squarePortion.GetCurrentColorIndex())
		{
			bottomDestroyed = true;
			bottom.QueueFree();
		}

		if (topDestroyed && bottomDestroyed)
		{
			this.EmitSignal(nameof(SquareMobDestroyed), new object[] { this });
		}
	}

	/**
	 * <inheritdoc/>
	 */

	public void DestroyMob()
	{
		this.QueueFree();
	}

	/**
	 * <inheritdoc/>
	 */
	void Mob.OnMobCreated(GameContext gameContext)
	{
		this.nearestPlayer = Utils.GetNearestPlayer(this, gameContext.GetAllPlayers());
	}

	/**
	 * <inheritdoc/>
	 */
	void Mob.OnPlayerDestroyed(Player destroyedPlayer, GameContext gameContext)
	{
		// Since a square takes 2 hits to be destroyed, we need to make it follow the next player
		if (nearestPlayer == destroyedPlayer)
		{
			nearestPlayer = Utils.GetNearestPlayer(this, gameContext.GetAllPlayers());
		}
	}

	/**
	 * <inheritdoc/>
	 */

	/**
	 * Interface for parts of the <see cref="SquareMob"/>.
	 */
	public interface SquarePortion {

		/**
		 * Return true if <see cref="TopOfSquareMob"/>, false if <see cref="BottomOfSquareMob"/>.
		 */
		bool IsTop();

		/**
		 * Destroy portion and cleanup resources.
		 */
		void DestroyPortion();

		/**
		 * Return the index of the color in <see cref="Globals.COLORS"/> that can destroy the portion.
		 */
		int GetCurrentColorIndex();
	}
}
