using Godot;
using System;
using System.Collections.Generic;

/**
 * <summary>
 * A type of mob that can survive multiple shots.  It can land anywhere on the screen (will have an indicator when landing.  while landing it cannot take or deal any damage).
 * The shape is an Octagon.  It will shoot bullets radially outward.  Can be destroyed by any color.
 * 
 * The mob is white to indicate it can be destroyed and has a health indicator in which parts of it will be turned
 * a color if it was hit by the color (when the full shape is colored, then it is destroyed).
 * </summary>
 * 
 * The way we will "animate" landing is that it will from a small black point into a full sized Octagon.  It will then change colors to indicate that is has "landed".
 */
public class OctagonMob : Area2D, Mob, PlayerDamager
{
	/**
	 * Time in seconds that the mob will take to spawn.  Within this time, it cannot damage or be damaged.
	 */
	private const float TIME_TO_SPAWN_S = 2;

	/**
	 * The x-axis position of the center of the player, relative to anchor.
	 * Mainly used for debugging (i.e. parent anchor is 0,0 so if we want the whole object to be within the screen
	 * for quick inspection, we can make this a positive value).
	 */
	private float centerX = 0;

	/**
	 * The y-axis position of the center of the player, relative to anchor.
	 * Mainly used for debugging (i.e. parent anchor is 0,0 so if we want the whole object to be within the screen
	 * for quick inspection, we can make this a positive value).
	 */
	private float centerY = 0;

	/**
	 * When we reach this radius, we have "landed."
	 */
	private float maxRadius = 30;

	/**
	 * Keeps track of the current radius for the landing animation.
	 */
	private float preSpawnCurrentRadius = 0;

	/**
	 * A flag to indicate whether it's still spawning, to be used for the landing animation.
	 */
	private bool isStillSpawning = true;

	/**
	 * A flag to indicate that we need to add the collision layer.  It will be toggled so we only add the collision layer when we've landed.
	 */
	private bool addCollisionLayer = false;

	/**
	 * Used to create a <see cref="OctagonBullet"/>.
	 */
	private PackedScene octagonBulletScene = (PackedScene)GD.Load("res://mob/octagon/OctagonBulletScene.tscn");

	/**
	 * Timer to keep track of how frequently the mob can fire bullets that can damage the player.
	 */
	private Timer fireTimer;

	/**
	 * A flag to indicate whether the mob can fire bullets again.
	 */
	private bool canFire = true;

	/**
	 * Counter that should change every time the Octagon fires.
	 * Will be used to add slight rotation offsets to the bullet fired so that a player cannot avoid the bullets by standing in one spot.
	 */
	private int numFires = 0;

	/**
	 * Keeps tracks of the colors that hit this mob (which serves as the mob's health and is used to display
	 * the damage dealt).
	 */
	private List<int> colorIndexesThatHitMob = new List<int>();

	/**
	 * Signal that indicates that the mob was hit (with the mob instance that was hit and the bullet that hit it).
	 */
	[Signal]
	public delegate void OctagonMobDestroyed(Node mob, Bullet bullet);

	/**
	 * Signal that indicates that the mob was destroyed (with the mob instance that was destroyed and the bullet that destroyed it).
	 */
	[Signal]
	public delegate void OctagonMobHit(Node portion, Bullet bullet);

	public override void _Ready()
	{
		this.Connect("body_entered", this, nameof(OnBodyEntered));

		fireTimer = new Timer();
		fireTimer.OneShot = true;
		fireTimer.WaitTime = 1;
		this.AddChild(fireTimer);
		fireTimer.Connect("timeout", this, nameof(setCanFire));
		fireTimer.Start();
	}

	public override void _Draw()
	{
		base._Draw();

		// Octagon is 8 triangles where the corners join in the center.  Those corner degrees are 45 degrees (360 / 8)
		// Therefore, we can generate the octagon by placing an equidistant point from the center every 45 degrees
		Vector2[] points = new Vector2[8];
		for (int i = 0; i < 8; i++)
		{
			double angle = (i * 45.0 * 2 * Math.PI / 360.0);
			double x = preSpawnCurrentRadius * Math.Cos(angle);
			double y = preSpawnCurrentRadius * Math.Sin(angle);
			Vector2 vector2 = new Vector2((float)x + centerX, (float)y + centerY);
			points[i] = vector2;
		}

		// First draw the polygon
		Color[] fillColor = new Color[] { isStillSpawning ? Utils.BLACK : Utils.WHITE };
		this.DrawPolygon(points, fillColor);

		// Then, draw the damage indicator on top
		for (int i = 0; i < this.colorIndexesThatHitMob.Count; i++)
		{
			Vector2[] triangle = new Vector2[]
			{
			new Vector2(centerX, centerY),
			points[i],
			points[(i + 1) % 8]
			};
			this.DrawPolygon(triangle, new Color[] { Globals.COLORS[colorIndexesThatHitMob[i]] });;
		}

		// Finally, draw the border
		Utils.DrawBorder(this, points, Utils.BLACK);

		if (addCollisionLayer)
		{
			ConvexPolygonShape2D convexPolygonShape2D = new ConvexPolygonShape2D();
			convexPolygonShape2D.Points = points;
			CollisionShape2D collisionShape2D = new CollisionShape2D();
			collisionShape2D.Shape = convexPolygonShape2D;
			Utils.ReplaceCollisionChild(this, "COLLISION_SHAPE", collisionShape2D);
			addCollisionLayer = false;
		}
	}

	public override void _Process(float delta)
	{
		base._Process(delta);

		if (isStillSpawning)
		{
			// delta on its own will increase radius by 1 px per sec (with the delta).
			// if we want it to get to 30 px (radius) in 2s, we need to multiply by delta with the desired rate.
			preSpawnCurrentRadius += delta * maxRadius / TIME_TO_SPAWN_S;

			// Toggle values so that spawn status doesn't change if radius changes (say, due to resize)
			if (preSpawnCurrentRadius >= maxRadius)
			{
				isStillSpawning = false;
				addCollisionLayer = true;
			}
			this.Update();
		}
		else if (canFire)
		{
			// There are subrotations in the radial bullet storm so that player cannot stay in one place to dodge the bullets
			const int subRotations = 16;
			for (int i = 0; i < 8; i++)
			{
				float angle = (2 * (float)Math.PI * i) / 8 + ((float)Math.PI * numFires / subRotations);
				float x = (float)Math.Cos(angle);
				float y = (float)Math.Sin(angle);
				OctagonBullet bullet = (OctagonBullet)octagonBulletScene.Instance();
				bullet.NormalizedDirection = new Vector2(x, y);
				bullet.Rotation = angle;
				// TODO: This is a weird abstraction...assuming that parent is Main/screen
				bullet.Position = this.Position;
				this.GetParent().AddChild(bullet);
			}
			canFire = false;
			fireTimer.Start();
			numFires = (numFires + 1) % subRotations;
		}
	}

	private void setCanFire()
	{
		canFire = true;
	}

	/**
	 * Process collision with this object.
	 */
	public void OnBodyEntered(Node body)
	{
		if (body.GetType() == typeof(Bullet))
		{
			this.EmitSignal(nameof(OctagonMobHit), new object[] { this, body });
			Bullet bullet = (Bullet)body;
			lock (colorIndexesThatHitMob)
			{
				colorIndexesThatHitMob.Add(bullet.CurrentColorIndex);
			}
			this.Update();
			if (colorIndexesThatHitMob.Count == 8)
			{
				this.EmitSignal(nameof(OctagonMobDestroyed), new object[] { this, body });
			}
		}
	}

	/**
	 * Get the max radius so that it can be used in calculations in other objects
	 * (for example, we want to spawn this in full view of the UI and not partially offscreen, which requires the radius).
	 */
	public float getMaxRadius()
	{
		return maxRadius;
	}

	/**
	 * <inheritdoc/>
	 */
	void Mob.DestroyMob()
	{
		this.QueueFree();
	}

	void Mob.OnMobCreated(GameContext gameContext)
	{
	}

	void Mob.OnPlayerDestroyed(Player destroyedPlayer, GameContext gameContext)
	{
	}
}
