using Godot;
using System;

/**
 * <summary>
 * The bullet that the <see cref="OctagonMob"/> can fire.  It can damage the player, but cannot be destroyed by player.  The shape is a triangle.
 * </summary>
 */
public class OctagonBullet : Area2D, PlayerDamager
{
	/**
	 * Velocity of the bullet in pixels per second.
	 */
	private const float TRAVEL_PIXELS_PER_S = 400;

	/**
	 * The x-axis position of the center of the player, relative to anchor.
	 * Mainly used for debugging (i.e. parent anchor is 0,0 so if we want the whole object to be within the screen
	 * for quick inspection, we can make this a positive value).
	 */
	private float centerX = 0;

	/**
	 * The y-axis position of the center of the player, relative to anchor.
	 * Mainly used for debugging (i.e. parent anchor is 0,0 so if we want the whole object to be within the screen
	 * for quick inspection, we can make this a positive value).
	 */
	private float centerY = 0;

	/**
	 * We will construct an equilateral triangle for the bullet shape, and this is the length
	 * of one side.
	 */
	private float triangleLength = 7;

	/**
	 * Variable that stores the current screensize.  Just imitating the tutorial and is probably a
	 * premature optimization (could probably be easier just to call <see cref="Node.GetViewport"/>).
	 * Just copying Godot documentation here.
	 */
	private Vector2 screenSize;

	/**
	 * Keep track of distance traveled so we can remove bullet when it's out of sight.
	 */
	private float distanceTraveled = 0;

	/**
	 * REQUIRED DURING INSTANTIATION.  Indicates the direction the bullet should be facing when instantiated.
	 * Must be normalized using a method like <see cref="Vector2.Normalized"/>.
	 */
	public Vector2 NormalizedDirection
	{
		get;
		set;
	}

	public override void _Ready()
	{
		screenSize = GetViewport().Size;
	}

	public override void _Draw()
	{
		base._Draw();

		// Note that bullet position is relative to the parent (currently the mob itself)
		Vector2 topLeft = new Vector2(centerX, centerY - triangleLength);
		Vector2 right = new Vector2(centerX + (triangleLength * (float)Math.Sqrt(3)), centerY);
		Vector2 bottomLeft = new Vector2(centerX, centerY + triangleLength);

		Color[] fillColor = new Color[] { Utils.BLACK };
		Vector2[] triangleVertices = { right, topLeft, bottomLeft };
		this.DrawPolygon(triangleVertices, fillColor);

		Utils.DrawBorder(this, triangleVertices, Utils.BLACK);

		ConvexPolygonShape2D convexPolygonShape2D = new ConvexPolygonShape2D();
		convexPolygonShape2D.Points = triangleVertices;
		CollisionShape2D collisionShape2D = new CollisionShape2D();
		collisionShape2D.Shape = convexPolygonShape2D;
		Utils.ReplaceCollisionChild(this, "CollisionShape", collisionShape2D);
	}

	public override void _Process(float delta)
	{
		base._Process(delta);
		Vector2 moveVect = NormalizedDirection * TRAVEL_PIXELS_PER_S * delta;
		this.Position += moveVect;
		distanceTraveled += moveVect.Length();
		// Don't let the bullet travel forever (can remove when out of bounds)
		if (distanceTraveled >= (screenSize.x + screenSize.y))
		{
			this.QueueFree(); // TODO: emit a signal instead
		}
	}
}
