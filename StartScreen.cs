using Godot;
using System;
using System.Linq;

/**
 * <summary>
 * Entry point for players.  When the game first launches, this is what they will see.
 * </summary>
 */
public class StartScreen : Node
{
	/**
	 * We currently constrain the number of players so that the screen isn't filled.
	 */
	private const int MAX_NUM_PLAYERS = 4;

	/**
	 * See <see cref="Globals"/>.
	 */
	private Globals globals;

	/**
	 * Used to mutate <see cref="Globals.PlayerInputMappings"/>.
	 */
	private readonly object playerInputMappingsLock = new object();

	public override void _Ready()
	{
		globals = Globals.LoadGlobals(this);
	}

	/**
	 * <remarks>
	 * Note that the current strategy has a few problems
	 * - If Steam (or any other system which causes joystick to act as a keyboard) is running, then a joystick button press is also going to emit
	 *   a keyboard press event).  This can be fixed if we could determine the difference between joystick and keyboard.  Unfortunately, it doesn't
	 *   look like Godot offers a way to do this right now (July 2020).  Both keyboard and joystick have deviceId of 0.
	 * </remarks>
	 */
	public override void _Input(InputEvent @event)
	{
		// Note the cast is safer than trying to turn the scancode into a KeyList since the cast will always work, but unclear if scancode
		// will always map
		if (@event is InputEventKey keyboardButton && keyboardButton.IsPressed())
		{
			if (keyboardButton.Scancode == (uint)KeyList.Enter && globals.PlayerInputMappings.Count < MAX_NUM_PLAYERS)
			{
				GD.Print("Adding kb+m ", keyboardButton.Device, " ; device name: ", Input.GetJoyName(keyboardButton.Device));
				lock (playerInputMappingsLock)
				{
					bool isMouseAlreadyMapped = globals.PlayerInputMappings.Any((x) => { return x.IsMouse && x.DeviceId == keyboardButton.Device; });
					if (!isMouseAlreadyMapped)
					{
						globals.PlayerInputMappings.Add(PlayerInputMapping.CreateKeyboardMouseInputMapping(keyboardButton.Device));
						// Add player (try to avoid race condition)
						getPlayerListLabel().Text += String.Format("\n Player {0}: Keyboard & Mouse", globals.PlayerInputMappings.Count());
						getStartGameLabel().Visible = true;
					}
				}
			}
			else if (keyboardButton.Scancode == (uint)KeyList.Space)
			{
				bool isMouseAlreadyMapped = false;
				lock (playerInputMappingsLock)
				{
					isMouseAlreadyMapped = globals.PlayerInputMappings.Any((x) => { return x.IsMouse && x.DeviceId == keyboardButton.Device; });
				}

				if (isMouseAlreadyMapped)
				{
					startGame();
				}
			}
		}
		else if (@event is InputEventJoypadButton controllerButton && controllerButton.IsPressed())
		{
			if (controllerButton.ButtonIndex == (int)JoystickList.XboxA && globals.PlayerInputMappings.Count < MAX_NUM_PLAYERS)
			{
				GD.Print("Adding XBOX controller ", controllerButton.Device, " ; device name: ", Input.GetJoyName(controllerButton.Device));
				lock (playerInputMappingsLock)
				{
					bool isJoyAlreadyMapped = globals.PlayerInputMappings.Any((x) => { return !x.IsMouse && x.DeviceId == controllerButton.Device; });
					if (!isJoyAlreadyMapped)
					{
						globals.PlayerInputMappings.Add(PlayerInputMapping.CreateJoyInputMapping(controllerButton.Device));
						// Add player (try to avoid race condition)
						getPlayerListLabel().Text += String.Format("\n Player {0}: Joystick {1}", globals.PlayerInputMappings.Count(), controllerButton.Device);
						getStartGameLabel().Visible = true;
					}
				}
			}
			else if (controllerButton.ButtonIndex == (int)JoystickList.Start)
			{
				bool isJoyAlreadyMapped = false;
				lock (playerInputMappingsLock)
				{
					isJoyAlreadyMapped = globals.PlayerInputMappings.Any((x) => { return !x.IsMouse && x.DeviceId == controllerButton.Device; });
				}
				if (isJoyAlreadyMapped)
				{
					startGame();
				}
			}
		}
	}

	private void startGame()
	{
		this.GetTree().ChangeScene("res://MainScene.tscn");
	}

	private Label getPlayerListLabel()
	{
		// Manually created through UI
		return this.GetNode<Label>("Panel/Players");
	}

	private Label getStartGameLabel()
	{
		// Manually created through UI
		return this.GetNode<Label>("Panel/StartText");
	}
}
